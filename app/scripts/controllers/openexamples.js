'use strict';

SwaggerEditor.controller('OpenExamplesCtrl', function OpenExamplesCtrl($scope,
  $modalInstance, $rootScope, $state, FileLoader, Builder, Storage, Analytics,
  defaults) {

  $scope.files = defaults.exampleFiles;
  $scope.selectedFile = defaults.exampleFiles[0];

  $scope.open = function (file) {

    // removes trailing slash from pathname because examplesFolder always have a
    // leading slash
    var pathname = _.endsWith(location.pathname, '/') ?
      location.pathname.substring(1) :
      location.pathname;

    var url = '/' + pathname + defaults.examplesFolder + file;

    FileLoader.loadFromUrl(url).then(function (value) {
      Storage.save('yaml', value);
      $rootScope.editorValue = value;
      $state.go('home', {tags: null});
      $modalInstance.close();
    }, $modalInstance.close);

    Analytics.sendEvent('open-example', 'open-example:' + file);
  };

  $scope.cancel = $modalInstance.close;
});

SwaggerEditor.controller('OpenSpecExamplesCtrl', function OpenSpecExamplesCtrl($scope,$http,
  $modalInstance, $rootScope, $state, FileLoader, Builder, Storage,
  Analytics, YAML, defaults,awsServices) {

	awsServices.getSwaggerSpecs ($scope);
	
	 $scope.open = function (file) {
				Analytics.sendEvent('open-example', 'open-example:' + file);

				$http.get('config/awsProperties.json').
				  success(function(data, status, headers, config) {	
					
					awsServices.setupGetData ($scope,file)	
					var urlToRetrieveObject = awsServices.getAwsUrl($scope,data,'getObject');
					$http.get(urlToRetrieveObject).success(function(data, status, headers, config){
								
								var yaml = JSON.stringify(data);
								YAML.load(yaml, function (error, json) {

								      // if `yaml` is JSON, convert it to YAML
								      var jsonParseError = null;
								      try {
								        JSON.parse(yaml);
								      } catch (error) {
								        jsonParseError = error;
								      }

								      if (!jsonParseError) {
								        YAML.dump(json, function (error, yamlStr) {
								          assign(yamlStr, json);
								        });
								      } else {
								        assign(yaml, json);
								      }
								      function assign(yaml, json) {
											Analytics.sendEvent('open-example', 'open-example:' + file);
											Storage.save('yaml', yaml);
			  								$state.go('home', {tags: null});
			  								$rootScope.editorValue = yaml;
			 					 			$modalInstance.close();        
								      }
								    });
							}).
							error(function(data, status, headers, config) {
    							alert("Error status : "+status);
  							});
				});		
	};

	$scope.cancel = $modalInstance.close;
});
